<?php

namespace Tetrapak07\Multilang;

use Phalcon\Mvc\Controller;
use Supermodule\ControllerBase as SupermoduleBase;
use Tetrapak07\Multilang\DBTranslateAdapter;

class MultilangBase extends Controller
{
    public $defaultLang;   
    public $translation = null;
    
    public function initialize()
    {

    }
    
    public function onConstruct()
    {
        $this->dataReturn = true;
        $this->defaultLang = $this->config->modules->multilang->defalultLanguage;
       
        if (!SupermoduleBase::checkAndConnectModule('multilang')) {
            $this->dataReturn = false;
            $this->bestLang = $this->defaultLang;
        } else {
            $this->bestLang = $this->request->getBestLanguage(); 
        } 
        $this->translation = $this->_getTranslation(); 
    }
    
    protected function _getTranslation()
    {
        if ($this->dataReturn) {
        return new DBTranslateAdapter([
            'db'                     => $this->di->get('db'), // Here we're getting the database from DI
            'table'                  => 'translations', // The table that is storing the translations
            'language'               => $this->bestLang, // Now we're getting the best language for the user
            'useIcuMessageFormatter' => true, // Optional, if need formatting message using ICU MessageFormatter
        ]);
        }
        return false;       
    }
         
   public function setLanguageData($entity, $entityID, $data, $lang = false) 
   {
        if (!is_array($data)OR(empty($data))OR(count($data) == 0)) {
          return false;   
        }
        foreach ($data as $key => $value) {
            $this->translation->offsetSet($key, $this->filter->sanitize($value, ["trim", "string", "striptags"]), $entity, $entityID, $lang); 
        }
   }
   
   public function getLanguageData($entity, $entityID, $lang = false) 
   {   
     return $this->translation->offsetGetAll($entity, $entityID, $lang);  
   }
   
    public function getLanguageDataKey($translateKey, $type = '', $id = 0, $lang = false) 
    {
         return $this->translation->offsetGet($translateKey, $type, $id, $lang);
    }
}    
